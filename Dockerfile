FROM golang:alpine
WORKDIR /workspace
COPY go.mod .
RUN go mod download -json
COPY main.go .
RUN CGO_ENABLED=0 go build -o hello-server

USER 1001

CMD ["/workspace/hello-server"]
